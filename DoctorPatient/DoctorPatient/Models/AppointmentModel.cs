﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoctorPatient.Models
{
    public class AppointmentModel : IComparable<AppointmentModel>
    {
        [Key]
        public int AppointmentID { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserID { get; set; }

        [Required] //Changes V2
        public int DoctorID { get; set; }

        [Required(ErrorMessage = "Please enter your pet's name")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Please enter a pet name between 3 and 50 characters in length.")]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$", ErrorMessage = "Please enter a pet name beginning with a capital letter and spaces only.")]
        [DisplayName("Pet's Name")]
        public string PetName { get; set; }

        [Display(Name = "Type of pet")]
        [Required(ErrorMessage = "Please select the type of pet that you would like to book an appointment for!")]
        public Enums.PetType PetType { get; set; }
        [Display(Name = "Type of breed")]
        public Enums.PetBreed PetBreed { get; set; }

        [Required]
        [Display(Name = "Date for Appointment")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [MyAppointmentDateValidation(ErrorMessage = "Are you creating an appointment for the past?")]
        public DateTime Date { get; set; }

        //Disabling due to variable appointment times now. 
        //[MyTimeValidation(ErrorMessage="Appointments only available for HH:00 and HH:30")]
        [DataType(DataType.Time)]
        public DateTime Time { get; set; }

        public string TimeBlockHelper { get; set; }

        public virtual DoctorModel Doctor { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public int CompareTo(AppointmentModel other)
        {
            return this.Date.Date.Add(this.Time.TimeOfDay).CompareTo(other.Date.Date.Add(other.Time.TimeOfDay));
        }
    }
}