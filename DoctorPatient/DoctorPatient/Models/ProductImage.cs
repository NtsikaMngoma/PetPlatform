﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DoctorPatient.Models
{
    public class ProductImage
    {
        public int Id { get; set; }
        [DisplayName("File")]
        [StringLength(100)]
        [Index(IsUnique = true)]
        public string FileName { get; set; }
        public virtual ICollection<ProductImageMapping> ProductImageMappings { get; set; }
    }
}