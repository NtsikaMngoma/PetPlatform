﻿
using System.ComponentModel;

namespace DoctorPatient.Models
{
    public class Enums
    {
        public enum Sex
        {
            Male, Female
        }

        public enum HospitalDepartment
        {
            Surgery,
            Neurology,
            Anesthesia,
            Gynecology,
            Pediatrics,
            Dentistry,
            Emergency,
            Nursing,
            Pharmacy
        }

        public enum DoctorDegree
        {
            MBBS, FCPS, Diploma, FRCPS
        }
        public enum PetType
        {
            Dogs, 
            Rabbits,
            Cats,
            Mice,
            Birds,
            Livestock,
            Other
        }

        public enum PetBreed
        {
            [Description("Dogs")]
            Azawakh,
            Boerboel,
            Ridgeback,
            Rottweiller,
            Spaniels,
            Sloughi,
            Terrier,
            Mixed,
            Other,
            Foreign,

            [Description("Rabbits")]
            Lop,
            Femish,
            Angora,
            Hotot,
            Hare,
            Beveren,
            Himalayan,

            [Description("Cats")]
            Abyssinian,
            Bengal,
            Burmese,
            Cornish,
            Curl,
            Devon,
            Maine,
            Oriental,
            Persian,
            Ragdoll,
            Siamese,
            Tonkinese,

            [Description("Mice")]
            Jungle, 
            Hamster,
            

            [Description("Birds")]
            Lovebird,
            Parrot,
            Canary,
            Noddy,

            [Description("Livestock")]
            Pig,
            Ovis,
            Goat,
            Horse,
            Donkey,
            Mule,
            Buffalo

        }
    }
}