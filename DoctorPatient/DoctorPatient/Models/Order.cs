﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoctorPatient.Models
{
    public class Order
    {
        [DisplayName("Order Id")]
        public int OrderId { get; set; }
        [DisplayName("User")]
        public string UserId { get; set; }
        [DisplayName("Deliver to")]
        public string DeliveryName { get; set; }
        public Address DeliveryAddress { get; set; }
        [DisplayName("Total Price")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal TotalPrice { get; set; }
        [DisplayName("Time of Order")]
        public DateTime DateCreated { get; set; }
        public List<OrderLine> OrderLines
        {
            get; set;
        }
    }
}