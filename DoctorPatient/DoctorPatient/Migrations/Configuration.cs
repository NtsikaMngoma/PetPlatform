namespace DoctorPatient.Migrations
{
    using DoctorPatient.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Globalization;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DoctorPatient.Infrastructure.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DoctorPatient.Infrastructure.ApplicationDbContext context)
        {
            var admin = new List<AdministrationModel>
            {
                 new AdministrationModel { Name = "Appointment Duration in Minutes [Default:30]",   Value = "30"},
                 new AdministrationModel { Name = "Working Hours Start in 24-Hour Format [Default:8]",   Value = "8"},
                 new AdministrationModel { Name = "Working Hours End in 24-Hour Format [Default:18]",   Value = "18"},
            };
            admin.ForEach(s => context.Administrations.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

            var categories = new List<Category>
            {
                new Category {Name = "Dogs"},
                new Category {Name = "Cats"},
                new Category {Name = "Rabbits"},
                new Category {Name = "Birds"}
            };
            categories.ForEach(c => context.Categories.AddOrUpdate(p => p.Name, c));
            context.SaveChanges();

            //var products = new List<Product>
            //{
            //    new Product {Name = "Cool Bite", Description = "Cold meat for aggressive dogs, especially terriers", Price = 200.95m, CategoryId = categories.Single(c => c.Name == "Dogs").Id},
            //    new Product {Name = "Miaow wow", Description = "The best milk for your ugly cat", Price = 56.95m, CategoryId = categories.Single(c => c.Name == "Cats").Id},
            //    new Product {Name = "Leaves and all", Description = "The best food for your annoying rabbit", Price = 200.95m, CategoryId = categories.Single(c => c.Name == "Rabbits").Id},
            //    new Product {Name = "Fly high", Description = "The best bird feed, infused with marijuana and ecstasy. It will make your parrot laugh out loud, fly to the moon and back, and die after waking up. Lol!", Price = 1000.95m, CategoryId = categories.Single(c => c.Name == "Birds").Id},
            //};
            //products.ForEach(c => context.Products.AddOrUpdate(p => p.Name, c));
            //context.SaveChanges();

            //Admin
            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Admin" };


                manager.Create(role);
            }
            // if user doesn't exist, create one and add it to the admin role
            if (!context.Users.Any(u => u.UserName == "Admin"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser
                {
                    UserName = "markmngoma54@gmail.com",
                    Email = "markmngoma54@gmail.com",
                    Name = "Ntsika Mngoma",
                    BirthDate = new DateTime(1997, 05, 01),
                    Address = new Address
                    {
                        AddressLine1 = "90 Umhlanga Ridge",
                        Town = "Umhlanga",
                        Province = "KZN",
                        PostCode = "4004"
                    }
                };

                manager.Create(user, "password101@user");
                manager.AddToRole(user.Id, "Admin");
            }

            //Patient
            if (!context.Roles.Any(r => r.Name == "Patient"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Patient" };


                manager.Create(role);
            }
            // if user doesn't exist, create one and add it to the patient role
            if (!context.Users.Any(u => u.UserName == "Patient"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser
                {
                    UserName = "ntsikamngoma@gmail.com",
                    Email = "ntsikamngoma@gmail.com",
                    Name = "Ntsika Mngoma",
                    BirthDate = new DateTime(1997, 05, 01),
                    Address = new Address
                    {
                        AddressLine1 = "90 Umhlanga Ridge",
                        Town = "Umhlanga",
                        Province = "KZN",
                        PostCode = "4004"
                    }
                };

                manager.Create(user, "Lepassiou101@");
                manager.AddToRole(user.Id, "Patient");
            }
        }
    }
}

