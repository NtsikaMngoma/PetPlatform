﻿using DoctorPatient;
using DoctorPatient.Infrastructure;
using DoctorPatient.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DoctorPatient.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Orders
        public ActionResult Index(string orderSearch, string startDate, string endDate, string orderSortOrder)
        {
            var orders = db.Orders.OrderBy(o => o.DateCreated).Include(o => o.OrderLines);
            if (User.IsInRole("Patient"))
            {
                return View(db.Orders.Where(o => o.UserId == User.Identity.Name));
            }
            if (!String.IsNullOrEmpty(orderSearch))
            {
                orders = orders.Where(o => o.OrderId.ToString().Equals(orderSearch) ||
                o.UserId.Contains(orderSearch) || o.DeliveryName.Contains(orderSearch) ||
                o.DeliveryAddress.AddressLine1.Contains(orderSearch) ||
                o.DeliveryAddress.AddressLine2.Contains(orderSearch) ||
                o.DeliveryAddress.Town.Contains(orderSearch) ||
                o.DeliveryAddress.Province.Contains(orderSearch) ||
                o.DeliveryAddress.PostCode.Contains(orderSearch) ||
                o.TotalPrice.ToString().Equals(orderSearch) ||
                o.OrderLines.Any(ol => ol.ProductName.Contains(orderSearch)));
            }
            DateTime parsedStartDate;
            if (DateTime.TryParse(startDate, out parsedStartDate))
            {
                orders = orders.Where(o => o.DateCreated >= parsedStartDate);
            }
            DateTime parsedEndDate;
            if (DateTime.TryParse(endDate, out parsedEndDate))
            {
                orders = orders.Where(o => o.DateCreated <= parsedEndDate);
            }

            ViewBag.DateSort = String.IsNullOrEmpty(orderSortOrder) ? "date" : "";
            ViewBag.UserSort = orderSortOrder == "user" ? "user_desc" : "user";
            ViewBag.PriceSort = orderSortOrder == "price" ? "price_desc" : "price";
            ViewBag.CurrentOrderSearch = orderSearch;
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;

            switch (orderSortOrder)
            {
                case "user":
                    orders = orders.OrderBy(o => o.UserId);
                    break;
                case "user_desc":
                    orders = orders.OrderByDescending(o => o.UserId);
                    break;
                case "price":
                    orders = orders.OrderBy(o => o.TotalPrice);
                    break;
                case "price_desc":
                    orders = orders.OrderByDescending(o => o.TotalPrice);
                    break;
                case "date":
                    orders = orders.OrderBy(o => o.DateCreated);
                    break;
                default:
                    orders = orders.OrderByDescending(o => o.DateCreated);
                    break;
            }
            return View(orders);
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Include(o => o.OrderLines).SingleOrDefault(o => o.OrderId == id);
            if (order == null)
            {
                return HttpNotFound();
            }

            if (order.UserId == User.Identity.Name || User.IsInRole("Admin"))
            {
                return View(order);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
        }

        


        // GET: Orders/Create
        public ActionResult Review()
        {
            Cart cart = Cart.GetCart();
            Order order = new Order();
            order.UserId = User.Identity.Name;
            var userId = User.Identity.GetUserId();
            var currentUser = db.ApplicationUsers.Find(userId);
            //ApplicationUser user = await UserManager.FindByNameAsync(order.UserId);
            order.DeliveryName = currentUser.Name;
            order.DeliveryAddress = currentUser.Address;
            order.OrderLines = new List<OrderLine>();
            foreach (var cartLine in cart.GetCartLines())
            {
                OrderLine line = new OrderLine
                {
                    Product = cartLine.Product,
                    ProductId = cartLine.ProductId,
                    ProductName = cartLine.Product.Name,
                    Quantity = cartLine.Quantity,
                    UnitPrice = cartLine.Product.Price
                };
            }
            //if (ModelState.IsValid)
            //{
            //    var body = "Email from App dev ";
            //    var message = new MailMessage();
            //    message.To.Add(new MailAddress("appdevproject18@gmail.com"));
            //    message.Subject = "Pet Clinic & Shop (Your Order(s))";
            //    message.Body = string.Format(body, order.UserId, order.DeliveryName, order.DeliveryAddress,
            //        order.OrderLines);
            //    using (var smtp = new SmtpClient())
            //    {
            //        await smtp.SendMailAsync(message);
            //        return RedirectToAction("Your order details have been emailed");
            //    }
            //}
            order.TotalPrice = cart.GetTotalCost();
            return View(order);
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrderId,UserId,DeliveryName,DeliveryAddress,TotalPrice,DateCreated")] Order order)
        {
            if (ModelState.IsValid)
            {
                order.DateCreated = DateTime.Now;
                db.Orders.Add(order);
                db.SaveChanges();

                //add the orderlines to the database after creating the order
                Cart cart = Cart.GetCart();
                order.TotalPrice = cart.CreateOrderLines(order.OrderId);
                db.SaveChanges();
                return RedirectToAction("Details", new { id = order.OrderId });
            }

            return RedirectToAction("Review");
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderId,UserId,DeliveryName,DeliveryAddress,TotalPrice,DateCreated")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
