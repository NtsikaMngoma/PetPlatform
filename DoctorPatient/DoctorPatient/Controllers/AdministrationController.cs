﻿using DoctorPatient.Infrastructure;
using DoctorPatient.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace DoctorPatient.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdministrationController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Administration/
        public ActionResult Index()
        {
            return View(db.Administrations.OrderBy(model => model.Name).ToList());
        }

        // GET: /Administration/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdministrationModel Administrationmodel = db.Administrations.Find(id);
            if (Administrationmodel == null)
            {
                return View("Error");
            }
            return View(Administrationmodel);
        }

        // POST: /Administration/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Value")] AdministrationModel Administrationmodel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Administrationmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Administrationmodel);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id, Name, Value")] AdministrationModel admin)
        {
            if (ModelState.IsValid)
            {
                db.Administrations.Add(admin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(admin);
        }

        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    AdministrationModel admin = db.Administrations.Find(id);
        //    if (admin == null)
        //    {
        //        return View("Error");
        //    }
        //    return View(admin);
        //}

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    AdministrationModel admin = db.Administrations.Find(id);
        //    foreach (var a in admin.)
        //    {
        //        a.
        //    }
        //}
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
