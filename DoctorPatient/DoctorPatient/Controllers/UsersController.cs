﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DoctorPatient.Infrastructure;
using DoctorPatient.Models;
using DoctorPatient.ViewModels;

namespace DoctorPatient.Controllers
{
    public class UsersController : Controller
    {
        private ApplicationDbContext db;

        public UsersController()
        {
            db = new ApplicationDbContext();
        }
        // GET: Users
        public ActionResult Index()
        {
            var users = (from u in db.Users
                select new UserViewModel
                {
                    Username = u.UserName,
                }).ToList();

            return View(users);
        }
        
    }
}