﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DoctorPatient
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "AdminEdit",
                url: "Administration/Edit",
                defaults: new { controller = "Administration", action = "Edit" }
            );

            routes.MapRoute(
               name: "ProductsCreate",
               url: "Products/Create",
               defaults: new { controller = "Products", action = "Create" }
           );

            routes.MapRoute(
                name: "ProductsByCategoryByPage",
                url: "Products/{category}/Page{page}",
                defaults: new { controller = "Products", action = "Index" }
            );

            routes.MapRoute(
                name: "ProductsByPage",
                url: "Products/Page{page}",
                defaults: new { controller = "Products", action = "Index" }
            );

            routes.MapRoute(
                name: "ProductsByCategory",
                url: "Products/{category}",
                defaults: new { controller = "Products", action = "Index" }
            );

            routes.MapRoute(
                name: "ProductsIndex",
                url: "Products",
                defaults: new { controller = "Products", action = "Index" }
            );

            routes.MapRoute(
                name: "ProductImages",
                url: "ProductImages",
                defaults: new { controller = "ProductImages", action = "Index" }
            );

            routes.MapRoute(
               name: "PaymentCancel",
               url: "PaymentCancel",
               defaults: new { controller = "Orders", action = "Cancel" }
            );

            routes.MapRoute(
               name: "PaymentSuccess",
               url: "PaymentSuccess",
               defaults: new { controller = "Orders", action = "Success" }
           );
        }
    }
}
