﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using Microsoft.Ajax.Utilities;

namespace DoctorPatient
{
    public class LambdaChapter84
    {
        public delegate int ModifyInt(int input);

        ModifyInt multiplyByTwo = x => x * 2;

        //the above code may be produced in this verbose way:
        private ModifyInt multiplyByTwo2 = delegate(int x) { return x * 2; };
    }

    public class Section843
    {
        delegate int ModifyInt(int input1, int input2);
        //Using parantheses around the expression to the left of the => operator to indicate multiple parameters
        private ModifyInt multiplyTwoInts = (x, y) => x * y;
        //This one is empty, specifying that the function does not parameters
        delegate string ReturnString();

        private ReturnString getGreeting = () => "Hello World";
    }
    //Section 84.4
    public class Person
    {
        //public string Name { get; set; }
        //public int Age { get; set; }
        ////The following lambda:
        ////p => p.Age > 18
        ////can be passed as an argument to both methods:
        //public void AsFunc(Func<Person, bool> func)
        //public void AsExpression(Expression<Func<Person, bool>> expr);

    }
}