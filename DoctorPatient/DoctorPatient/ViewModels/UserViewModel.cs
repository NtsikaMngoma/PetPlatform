﻿using DoctorPatient.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoctorPatient.ViewModels
{
    public class UserViewModel : EditUserViewModel
    {
        [Display(Name =  "User Name/E-Mail")]
        public string Username { get; set; }
    }
}