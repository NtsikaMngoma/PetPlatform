﻿using DoctorPatient.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoctorPatient.ViewModels
{
    public class CartViewModel
    {
        public List<CartLine> CartLines { get; set; }
        [DisplayName("Cart Total")]
        [DisplayFormat(DataFormatString = "{0:C} ")]
        public decimal TotalCost { get; set; }
    }
}