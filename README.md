# PetPlatform

An online booking system for your pet(s). It includes an ecommerce subsystem, containing a PayFast payment gateway. 
You are more than welcome to add more to the existing project.

Step 1:

- Clone the project: git clone PetPlatform url

Step 2:

- Update installed nuget packages on Visual Studio

Step 3: 

- Change connection string in Web.config

Step 4:

- Update database or add your own migrations.

Step 5:

- Customize the project and deploy on your cloud server.
